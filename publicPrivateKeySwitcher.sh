#!/bin/bash

# Get the script's current directory
script_dir=$(dirname "$(readlink -f "$0")")

# Define the new values for the variables
new_client_public_key="<New_Client_Public_Key>"
new_client_private_key="<New_Client_Private_Key>"

# Set the path to your Terraform configuration file
terraform_file="$script_dir/main.tf"

# Replace the values in the Terraform configuration file
sed -i "s/\${var.client_public_key}/$new_client_public_key/g" "$terraform_file"
sed -i "s/\${var.client_private_key}/$new_client_private_key/g" "$terraform_file"
